========================
 ABQ Data Entry Program
========================

Description
-----------
The program is being created to minimize data entry errors for laboratory measurements.
This program provides a data entry form for ABQ Agrilabs laboratory data.

Author
++++++
Konrad Poreba, 2019

Requirements
++++++++++++
* Python 3
* Tkinter

Usage
-----
To start the application, run::

  $ python3 abq_data_entry.py

General Notes
-------------

The CSV file will be saved to your current directory in the format "abq_data_record_CURRENTDATE.csv", where CURRENTDATE is today's date in ISO format.

This program only appends to the CSV file.  You should have a spreadsheet program installed in case you need to edit or check the file.

