import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from functools import partial


class GenericMainMenu(tk.Menu):
    """The Application's main menu"""

    def __init__(self, parent, settings, callbacks, **kwargs):
        """Constructor for MainMenu

        takes following arguments:
            parent (the parent widget)
            settings (a dictionary which contains Tkinter variables
            callbacks (a dictionary which contains Python callables
        """
        super().__init__(parent, **kwargs)
        self.settings = settings
        self.callbacks = callbacks
        self. _build_menu()
        self._bind_accelerators()

    @staticmethod
    def _argstrip(function, *args):
        return function()

    def _build_menu(self):

        file_menu = tk.Menu(self, tearoff=False)
        file_menu.add_command(label="Select file...",
                              command=self.callbacks['file->select'],
                              accelerator='Ctrl+O')
        file_menu.add_separator()
        file_menu.add_command(label="Quit",
                              command=self.callbacks['file->quit'],
                              accelerator='Ctrl+Q')
        self.add_cascade(label='File', menu=file_menu)

        options_menu = tk.Menu(self, tearoff=False)
        options_menu.add_checkbutton(label='Autofill Date',
                                     variable=self.settings['autofill date'])
        options_menu.add_checkbutton(label='Autofill Sheet data',
                                     variable=self.settings['autofill sheet data'])

        font_size_menu = tk.Menu(self, tearoff=False)
        for size in range(6, 17, 1):
            font_size_menu.add_radiobutton(label=size,
                                           value=size,
                                           variable=self.settings['font size'])
        options_menu.add_cascade(label='Font size', menu=font_size_menu)

        style = ttk.Style()
        themes_menu = tk.Menu(self, tearoff=False)
        for theme in style.theme_names():
            themes_menu.add_radiobutton(label=theme,
                                        value=theme,
                                        variable=self.settings['theme'])
        options_menu.add_cascade(label='Theme', menu=themes_menu)
        self.settings['theme'].trace('w', self.on_theme_change)
        self.add_cascade(label='Options', menu=options_menu)

        go_menu = tk.Menu(self, tearoff=False)
        go_menu.add_command(label="Record List",
                            command=self.callbacks['show_recordlist'],
                            accelerator='Ctrl+L')
        go_menu.add_command(label="New Record",
                            command=self.callbacks['new_record'],
                            accelerator='Ctrl+N')
        self.add_cascade(label='Go', menu=go_menu)

        help_menu = tk.Menu(self, tearoff=False)
        help_menu.add_command(label='About...', command=self.show_about)
        self.add_cascade(label='Help', menu=help_menu)

    def get_keybinds(self):
        return {
            '<Control-o>': self.callbacks['file->select'],
            '<Control-q>': self.callbacks['file->quit'],
            '<Control-n>': self.callbacks['new_record'],
            '<Control-l>': self.callbacks['show_recordlist'],
        }

    def _bind_accelerators(self):
        keybinds = self.get_keybinds()
        for key, command in keybinds.items():
            self.bind_all(key, partial(self._argstrip, command))

    def show_about(self):
        """Show the about dialog"""

        about_message = 'ABQ Data Entry'
        about_detail = ('by Konrad Poreba\n'
                        'For assistance please contact the author')
        messagebox.showinfo(title='About', message=about_message, detail=about_detail)

    def on_theme_change(self, *args):
        """Popup a message about theme changes"""

        message = "Change requires restart"
        detail = (
            "Theme changes do not take effect"
            " until application restart"
        )
        messagebox.showwarning(title='Warning',
                               message=message,
                               detail=detail)


class WindowsMainMenu(GenericMainMenu):

    def _build_menu(self):

        file_menu = tk.Menu(self, tearoff=False)
        file_menu.add_command(label="Select file...",
                              command=self.callbacks['file->select'],
                              accelerator='Ctrl+O')
        file_menu.add_separator()
        file_menu.add_command(label="Exit",
                              command=self.callbacks['file->quit'],
                              accelerator='Ctrl+Q')
        self.add_cascade(label='File', menu=file_menu)

        tools_menu = tk.Menu(self, tearoff=False)
        options_menu = tk.Menu(self, tearoff=False)
        options_menu.add_checkbutton(label='Autofill Date',
                                     variable=self.settings['autofill date'])
        options_menu.add_checkbutton(label='Autofill Sheet data',
                                     variable=self.settings['autofill sheet data'])

        font_size_menu = tk.Menu(self, tearoff=False)
        for size in range(6, 17, 1):
            font_size_menu.add_radiobutton(label=size,
                                           value=size,
                                           variable=self.settings['font size'])
        options_menu.add_cascade(label='Font size', menu=font_size_menu)

        style = ttk.Style()
        themes_menu = tk.Menu(self, tearoff=False)
        for theme in style.theme_names():
            themes_menu.add_radiobutton(label=theme,
                                        value=theme,
                                        variable=self.settings['theme'])

        options_menu.add_cascade(label='Theme', menu=themes_menu)
        self.settings['theme'].trace('w', self.on_theme_change)

        tools_menu.add_separator()
        tools_menu.add_cascade(label='Options', menu=options_menu)
        self.add_cascade(label='Tools', menu=tools_menu)

        self.add_command(label="Record List",
                         command=self.callbacks['show_recordlist'],
                         accelerator='Ctrl+L')

        self.add_command(label="New Record",
                         command=self.callbacks['new_record'],
                         accelerator='Ctrl+N')

        help_menu = tk.Menu(self, tearoff=False)
        help_menu.add_command(label='About...', command=self.show_about)
        self.add_cascade(label='Help', menu=help_menu)

    def get_keybinds(self):
        return {
            '<Control-o>': self.callbacks['file->select'],
            '<Control-n>': self.callbacks['new_record'],
            '<Control-l>': self.callbacks['show_recordlist'],
        }


class LinuxMainMenu(GenericMainMenu):

    def _build_menu(self):

        file_menu = tk.Menu(self, tearoff=False)
        file_menu.add_command(label="Select file...",
                              command=self.callbacks['file->select'],
                              accelerator='Ctrl+O')
        file_menu.add_separator()
        file_menu.add_command(label="Quit",
                              command=self.callbacks['file->quit'],
                              accelerator='Ctrl+Q')
        self.add_cascade(label='File', menu=file_menu)

        edit_menu = tk.Menu(self, tearoff=False)
        edit_menu.add_checkbutton(label='Autofill Date',
                                  variable=self.settings['autofill date'])
        edit_menu.add_checkbutton(label='Autofill Sheet data',
                                  variable=self.settings['autofill sheet data'])
        self.add_cascade(label='Edit', menu=edit_menu)

        view_menu = tk.Menu(self, tearoff=False)

        font_size_menu = tk.Menu(self, tearoff=False)
        for size in range(6, 17, 1):
            font_size_menu.add_radiobutton(label=size,
                                           value=size,
                                           variable=self.settings['font size'])
        view_menu.add_cascade(label='Font size', menu=font_size_menu)

        style = ttk.Style()
        themes_menu = tk.Menu(self, tearoff=False)
        for theme in style.theme_names():
            themes_menu.add_radiobutton(label=theme,
                                        value=theme,
                                        variable=self.settings['theme'])
        view_menu.add_cascade(label='Theme', menu=themes_menu)
        self.settings['theme'].trace('w', self.on_theme_change)
        self.add_cascade(label='Options', menu=view_menu)

        go_menu = tk.Menu(self, tearoff=False)
        go_menu.add_command(label="Record List",
                            command=self.callbacks['show_recordlist'],
                            accelerator='Ctrl+L')
        go_menu.add_command(label="New Record",
                            command=self.callbacks['new_record'],
                            accelerator='Ctrl+N')
        self.add_cascade(label='Go', menu=go_menu)

        help_menu = tk.Menu(self, tearoff=False)
        help_menu.add_command(label='About...', command=self.show_about)
        self.add_cascade(label='Help', menu=help_menu)


class MacOsMainMenu(GenericMainMenu):

    def _build_menu(self):

        app_menu = tk.Menu(self, tearoff=False, name='apple')
        app_menu.add_command(label='About ABQ Data Entry',
                             command=self.show_about)
        self.add_cascade(label='ABQ Data Entry', menu=app_menu)

        file_menu = tk.Menu(self, tearoff=False)
        file_menu.add_command(label="Select file...",
                              command=self.callbacks['file->select'],
                              accelerator='Cmd+O')
        file_menu.add_separator()
        self.add_cascade(label='File', menu=file_menu)

        edit_menu = tk.Menu(self, tearoff=False)
        edit_menu.add_checkbutton(label='Autofill Date',
                                  variable=self.settings['autofill date'])
        edit_menu.add_checkbutton(label='Autofill Sheet data',
                                  variable=self.settings['autofill sheet data'])
        self.add_cascade(label='Edit', menu=edit_menu)

        view_menu = tk.Menu(self, tearoff=False)

        font_size_menu = tk.Menu(self, tearoff=False)
        for size in range(6, 17, 1):
            font_size_menu.add_radiobutton(label=size,
                                           value=size,
                                           variable=self.settings['font size'])
        view_menu.add_cascade(label='Font size', menu=font_size_menu)

        style = ttk.Style()
        themes_menu = tk.Menu(self, tearoff=False)
        for theme in style.theme_names():
            themes_menu.add_radiobutton(label=theme,
                                        value=theme,
                                        variable=self.settings['theme'])
        view_menu.add_cascade(label='Theme', menu=themes_menu)
        self.settings['theme'].trace('w', self.on_theme_change)
        self.add_cascade(label='View', menu=view_menu)

        window_menu = tk.Menu(self, name='window', tearoff=False)
        window_menu.add_command(label="Record List",
                                command=self.callbacks['show_recordlist'],
                                accelerator='Cmd+L')
        window_menu.add_command(label="New Record",
                                command=self.callbacks['new_record'],
                                accelerator='Cmd+N')
        self.add_cascade(label='Window', menu=window_menu)

    def get_keybinds(self):
        return {
            '<Command-o>': self.callbacks['file->select'],
            '<Command-q>': self.callbacks['file->quit'],
            '<Command-n>': self.callbacks['new_record'],
            '<Command-l>': self.callbacks['show_recordlist'],
        }


def get_main_menu_for_os(os_name):
    menus = {
        'Linux': LinuxMainMenu,
        'Darwin': MacOsMainMenu,
        'freebsd7': LinuxMainMenu,
        'Windows': WindowsMainMenu
    }
    return menus.get(os_name, GenericMainMenu)