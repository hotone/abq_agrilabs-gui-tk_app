======================================
 ABQ Data Entry Program specification
======================================

Description
-----------
The program is being created to minimize data entry errors for laboratory measurements.
This program provides a data entry form for ABQ Agrilabs laboratory data.

Functionality Required
----------------------

The program must:

* allow all relevant, valid data to be entered, as per the field chart
* append entered data to a CSV file
    - The CSV file must have a filename of abq_data_record_CURRENTDATE.csv, where CURRENTDATE is the date of the checks in ISO format (Year-month-day)
    - The CSV file must have all the fields as per the chart
* enforce correct datatypes per field
* Provide a UI for reading, updating and appending data to the CSV file

The program should try, whenever possible, to:

* enforce reasonable limits on data entered
* Auto-fill data
* Suggest likely correct values
* Provide a smooth and efficient workflow
* have inputs that:
    - ignore meaningless keystrokes
    - require a value for all fields, except Notes
    - get marked with an error if the value is invalid on focusout
* prevent saving the record when errors are present



Functionality Not Required
--------------------------

The program does not need to:

* Allow deletion of data.

Limitations
-----------

The program must:

* Be efficiently operable by keyboard-only users.
* Be accessible to color blind users.
* Run on Debian Linux.
* Run acceptably on a low-end PC.

Data dictionary
---------------

+------------+----------+------+--------------+---------------------+
|Field       | Datatype | Units| Range        |Descripton           |
+============+==========+======+==============+=====================+
|Date        |Date      |      |              |Date of record       |
+------------+----------+------+--------------+---------------------+
|Time        |Time      |      |8, 12, 16, 20 |Time period          |
+------------+----------+------+--------------+---------------------+
|Lab         |String    |      | A - E        |Lab ID               |
+------------+----------+------+--------------+---------------------+
|Technician  |String    |      |              |Technician name      |
+------------+----------+------+--------------+---------------------+
|Plot        |Int       |      | 1 - 20       |Plot ID              |
+------------+----------+------+--------------+---------------------+
|Seed        |String    |      |              |Seed sample ID       |
|sample      |          |      |              |                     |
+------------+----------+------+--------------+---------------------+
|Fault       |Bool      |      |              |Fault on sensor      |
+------------+----------+------+--------------+---------------------+
|Light       |Decimal   |klx   | 0 - 100      |Light at plot        |
+------------+----------+------+--------------+---------------------+
|Humidity    |Decimal   |g/m³  | 0.5 - 52.0   |Abs humidity at plot |
+------------+----------+------+--------------+---------------------+
|Temperature |Decimal   |°C    | 4 - 40       |Temperature at plot  |
+------------+----------+------+--------------+---------------------+
|Blossoms    |Int       |      | 0 - 1000     |# blossoms in plot   |
+------------+----------+------+--------------+---------------------+
|Fruit       |Int       |      | 0 - 1000     |# fruits in plot     |
+------------+----------+------+--------------+---------------------+
|Plants      |Int       |      | 0 - 20       |# plants in plot     |
+------------+----------+------+--------------+---------------------+
|Max height  |Decimal   |cm    | 0 - 1000     |Ht of tallest plant  |
+------------+----------+------+--------------+---------------------+
|Min height  |Decimal   |cm    | 0 - 1000     |Ht of shortest plant |
+------------+----------+------+--------------+---------------------+
|Median      |Decimal   |cm    | 0 - 1000     |Median ht of plants  |
|height      |          |      |              |                     |
+------------+----------+------+--------------+---------------------+
|Notes       |String    |      |              |Miscellaneous notes  |
+------------+----------+------+--------------+---------------------+